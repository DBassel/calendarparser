package com.example.calendarparser;

import java.util.List;

import android.content.Context;
import android.widget.ArrayAdapter;

public class EventsListAdapter extends ArrayAdapter<CalendarEvent> {

	public EventsListAdapter(Context context, int resource,
			List<CalendarEvent> objects) {
		super(context, resource, objects);
	}

}
