package com.example.calendarparser;

import java.util.Date;

import android.database.Cursor;
import android.os.Bundle;

public class CalendarEvent {
	private Date startDate, endDate;
	private String title, description;
	private String place, id;
	
	public CalendarEvent(){
		
	}

	public CalendarEvent(Cursor cursor) {
		try {
			this.setId(cursor.getString(0));
			this.setTitle(cursor.getString(2));
			this.setDescription(cursor.getString(3));
			this.setStartDate(new Date((Long.parseLong(cursor.getString(4)))));
			this.setEndDate(new Date((Long.parseLong(cursor.getString(5)))));
			this.setPlace(cursor.getString(6));
		} catch (NumberFormatException nfe) {
			nfe.printStackTrace();
		}
	}

	public CalendarEvent(Bundle bundle) {
		this.setId(bundle.getString("id"));
		this.setStartDate(new Date(bundle.getLong("start_date")));
		this.setEndDate(new Date(bundle.getLong("end_date")));
		this.setTitle(bundle.getString("title"));
		this.setDescription(bundle.getString("description"));
		this.setPlace(bundle.getString("place"));
	}

	public Bundle toBundle() {
		Bundle b = new Bundle();
		b.putString("id", this.getId());
		b.putString("title", this.getTitle());
		b.putString("description", this.getDescription());
		b.putLong("start_date", this.getStartDate().getTime());
		b.putLong("end_date", this.getEndDate().getTime());
		b.putString("place", this.getPlace());
		return b;
	}

	public Date getStartDate() {
		return startDate;
	}

	private void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	private void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getTitle() {
		return title;
	}

	private void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	private void setDescription(String description) {
		this.description = description;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
