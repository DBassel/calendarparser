package com.example.calendarparser;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.text.format.DateFormat;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.calendarparser.EventManager.RemoteCallBack;

@SuppressLint("ValidFragment")
public class EventEditorActivity extends FragmentActivity {

	private CalendarEvent ce;
	private EventManager em;
	private EditText title, description, place;
	private Button startTime, endTime;
	private SimpleDateFormat dateFormat = new SimpleDateFormat(
			"HH:mm dd/MM/yyyy", new Locale("RU_ru"));
	private boolean called = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		called = false;

		em = new EventManager(getApplicationContext());

		ce = new CalendarEvent(getIntent().getExtras());

		fillDataToView();
	}

	private void fillDataToView() {
		setContentView(R.layout.event_editor);
		title = (EditText) findViewById(R.id.event);
		startTime = (Button) findViewById(R.id.start);
		endTime = (Button) findViewById(R.id.end);
		description = (EditText) findViewById(R.id.description);
		place = (EditText) findViewById(R.id.place);

		fillEvent(ce);
	}

	private void fillEvent(CalendarEvent event) {
		title.setText(event.getTitle());
		startTime.setText(dateFormat.format(event.getStartDate()));
		endTime.setText(dateFormat.format(event.getEndDate()));
		description.setText(event.getDescription());
		place.setText(event.getPlace());

		startTime.setOnClickListener(onDateClicked);
		endTime.setOnClickListener(onDateClicked);
	}

	private CalendarEvent getNewCE() {
		Bundle bundle = new Bundle();
		bundle.putString("id", ce.getId());
		try {
			bundle.putLong("start_date",
					dateFormat.parse(startTime.getText().toString()).getTime());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		try {
			bundle.putLong("end_date",
					dateFormat.parse(endTime.getText().toString()).getTime());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		bundle.putString("title", title.getText().toString());
		bundle.putString("description", description.getText().toString());
		bundle.putString("place", place.getText().toString());

		return new CalendarEvent(bundle);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.edit_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.save_event:
			em.updateEvent(getNewCE(), new RemoteCallBack() {
				@Override
				public void callBack() {
					handler.sendEmptyMessage(TOAST_OK);
					finish();
				}
			});
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	public static final int TOAST_OK = 0;
	@SuppressLint("HandlerLeak")
	private Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case TOAST_OK:
				toast("Event saved", Toast.LENGTH_SHORT);
				break;
			default:
				super.handleMessage(msg);
			}

		}
	};

	private void toast(String val, int duration) {
		Toast.makeText(getApplicationContext(), val, duration).show();
	}

	private OnClickListener onDateClicked = new OnClickListener() {
		@Override
		public void onClick(View v) {
			called = false;
			showDatePickerDialog(v);
		}
	};

	public class TimePickerFragment extends DialogFragment implements
			OnTimeSetListener {

		View viewButton = null;
		int year, month, day;

		public TimePickerFragment(View v, int year, int month, int day) {
			viewButton = v;
			this.year = year;
			this.month = month;
			this.day = day;

			called = true;
		}

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			// Use the current time as the default values for the picker
			final Calendar c = Calendar.getInstance();
			int hour = c.get(Calendar.HOUR_OF_DAY);
			int minute = c.get(Calendar.MINUTE);

			TimePickerDialog tpd = new TimePickerDialog(getActivity(), this,
					hour, minute, DateFormat.is24HourFormat(getActivity()));

			tpd.setCancelable(true);
			tpd.setCanceledOnTouchOutside(true);

			// Create a new instance of TimePickerDialog and return it
			return tpd;
		}

		@Override
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			((Button) viewButton).setText(String.valueOf(hourOfDay) + ":"
					+ String.valueOf(minute) + " " + String.valueOf(day) + "/"
					+ String.valueOf(month) + "/" + String.valueOf(year));
		}
	}

	public class DatePickerFragment extends DialogFragment implements
			OnDateSetListener {

		View viewButton = null;
		boolean bv = false;

		public DatePickerFragment(View v) {
			viewButton = v;
		}

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			// Use the current date as the default date in the picker
			final Calendar c = Calendar.getInstance();
			int year = c.get(Calendar.YEAR);
			int month = c.get(Calendar.MONTH);
			int day = c.get(Calendar.DAY_OF_MONTH);

			DatePickerDialog dpd = new DatePickerDialog(getActivity(), this,
					year, month, day);
			dpd.setCancelable(true);
			dpd.setCanceledOnTouchOutside(true);


			// Create a new instance of DatePickerDialog and return it
			return dpd;
		}

		@Override
		public void onDateSet(DatePicker view, int year, int month, int day) {
			if (!bv)
				showTimePickerDialog(viewButton, year, month + 1, day);
		}

		@Override
		public void onCancel(DialogInterface dialog) {
			super.onCancel(dialog);
			bv = true;
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			super.onDismiss(dialog);
			bv = true;
		}

	}

	private void showTimePickerDialog(View v, int year, int month, int day) {
		if (!called) {
			TimePickerFragment newFragment = new TimePickerFragment(v, year,
					month, day);
			newFragment.show(getSupportFragmentManager(), "timePicker");
		}
	}

	private void showDatePickerDialog(View v) {
		DatePickerFragment newFragment = new DatePickerFragment(v);
		newFragment.show(getSupportFragmentManager(), "datePicker");

		setRequestedOrientation(getResources().getConfiguration().orientation);
	}

}
