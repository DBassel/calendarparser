package com.example.calendarparser;

import java.text.SimpleDateFormat;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ListFragment;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter.ViewBinder;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.example.calendarparser.EventManager.RemoteCursorCallBack;

public class TaskListFragment extends ListFragment {

	private String timeFormat = "HH:mm";

	private Context context;

	private EventManager em;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		context = getActivity();

		em = new EventManager(getActivity());
		refreshList();
	};

	@Override
	public void onResume() {
		super.onResume();
		refreshList();
	}

	private void refreshList() {
		em.getRemoteEvents(new RemoteCursorCallBack() {
			@Override
			public void callBack(Cursor cursor) {
				handler.sendMessage(Message.obtain(null, FILL_EVENT_LIST,
						cursor));
			}
		});
	}

	public void fillAdapterWithData(Cursor cursor) {

		final SimpleDateFormat tf = new SimpleDateFormat(timeFormat,
				new Locale("ru_RU"));

		String[] fromColumns = { "_id", "title", "dtstart", "dtend" };
		int[] toViews = { R.id.no_view, R.id.event_name, R.id.start_date,
				R.id.end_date };

		SimpleCursorAdapter adapter = new SimpleCursorAdapter(context,
				R.layout.list_row, cursor, fromColumns, toViews, 0);

		adapter.setViewBinder(new ViewBinder() {
			@Override
			public boolean setViewValue(View aView, Cursor aCursor,
					int aColumnIndex) {

				if (aColumnIndex == 4 || aColumnIndex == 5) {
					TextView time = (TextView) aView;
					time.setText(tf.format(Long.parseLong(aCursor
							.getString(aColumnIndex))));
					return true;
				}
				return false;
			}
		});
		setListAdapter(adapter);
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);

		CalendarEvent ceToView = em.getEventById(id);

		Intent editIntent = new Intent(context, EventViewerAvtivity.class);

		editIntent.putExtras(ceToView.toBundle());
		startActivity(editIntent);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.update_events:
			refreshList();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	public static final int FILL_EVENT_LIST = 1;

	@SuppressLint("HandlerLeak")
	private Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case FILL_EVENT_LIST:
				fillAdapterWithData((Cursor) msg.obj);
				break;
			default:
				super.handleMessage(msg);
			}
		}
	};

}