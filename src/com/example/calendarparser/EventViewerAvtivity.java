package com.example.calendarparser;

import java.text.SimpleDateFormat;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.calendarparser.EventManager.RemoteEventCallBack;

public class EventViewerAvtivity extends Activity {

	private CalendarEvent ce;
	private EventManager em;
	private TextView title, startTime, endTime, description, place;
	private SimpleDateFormat dateFormat = new SimpleDateFormat(
			"HH:mm dd/MM/yyyy", new Locale("RU_ru"));

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		em = new EventManager(getApplicationContext());

		ce = new CalendarEvent(getIntent().getExtras());
		fillDataToView();
	}
	
	@Override
	protected void onRestart() {
		super.onRestart();
		
		refreshEvent();
	}
	
	private void refreshEvent(){
		em.getFreshEventById(ce.getId(), new RemoteEventCallBack() {
			@Override
			public void callBack(CalendarEvent event) {
				ce = event;
				handler.sendMessage(Message.obtain(null, UPDATE_EVENT, event));
			}
		});	
	}

	private void fillDataToView() {
		setContentView(R.layout.event_viewer);
		title = (TextView) findViewById(R.id.event);
		startTime = (TextView) findViewById(R.id.start);
		endTime = (TextView) findViewById(R.id.end);
		description = (TextView) findViewById(R.id.description);
		place = (TextView) findViewById(R.id.place);

		fillEvent(ce);
	}

	private void fillEvent(CalendarEvent event) {
		title.setText(event.getTitle());
		startTime.setText(dateFormat.format(event.getStartDate()));
		endTime.setText(dateFormat.format(event.getEndDate()));
		description.setText(event.getDescription());
		place.setText(event.getPlace());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.view_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.update_events:
			em.getFreshEventById(ce.getId(), new RemoteEventCallBack() {
				@Override
				public void callBack(CalendarEvent ce) {
					handler.sendMessage(Message.obtain(null, UPDATE_EVENT, ce));
				}
			});
			return true;
		case R.id.edit_event:
			
			Intent editIntent = new Intent(getApplicationContext(), EventEditorActivity.class);
			editIntent.putExtras(ce.toBundle());
			startActivity(editIntent);
			
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	public static final int UPDATE_EVENT = 1;

	@SuppressLint("HandlerLeak")
	private Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case UPDATE_EVENT:
				fillEvent((CalendarEvent) msg.obj);
				break;
			default:
				super.handleMessage(msg);
			}
		}
	};
}
