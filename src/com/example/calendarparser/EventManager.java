package com.example.calendarparser;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

public class EventManager {
	private ArrayList<CalendarEvent> events = new ArrayList<CalendarEvent>();

	public static final String[] columns = { "_id", "calendar_id", "title",
			"description", "dtstart", "dtend", "eventLocation" };

	private Context context;

	private Cursor cursor;

	public EventManager(Context context) {
		this.context = context;
	}

	public void getRemoteEvents(final RemoteCursorCallBack rcb) {

		new Thread() {
			@Override
			public void run() {
				super.run();
				String selectionClause = "(dtstart > ? and dtstart < ?)";

				String selectionArgs[] = {
						String.valueOf(getMidnight(0).getTime().getTime()),
						String.valueOf(getMidnight(1).getTime().getTime()) };
				
				Log.d("logg", String.valueOf(getMidnight(0).getTime().getTime() + " " + String.valueOf(getMidnight(1).getTime().getTime())));

				Cursor cursor = context.getContentResolver().query(
						Uri.parse("content://com.android.calendar/events"),
						columns, selectionClause, selectionArgs, "dtstart ASC");

				setCursor(cursor);

				toEventsList(cursor);

				if (rcb != null)
					rcb.callBack(cursor);
			}
		}.start();

	}

	private void toEventsList(final Cursor cursor) {
		events.clear();
		while (cursor.moveToNext()) {
			events.add(new CalendarEvent(cursor));
		}
		cursor.moveToPosition(-1);
	}

	public void getFreshEventById(final String id, final RemoteEventCallBack rcb) {
		new Thread() {
			@Override
			public void run() {
				super.run();

				Cursor tCursor;
				String selectionClause = "_id = ?";
				String selectionArgs[] = { id };

				tCursor = context.getContentResolver().query(
						Uri.parse("content://com.android.calendar/events"),
						columns, selectionClause, selectionArgs, null);
				if (tCursor.moveToFirst()) {
					CalendarEvent ce = new CalendarEvent(tCursor);

					if (rcb != null)
						rcb.callBack(ce);

				} else {
					if (rcb != null)
						rcb.callBack(null);
				}
			}
		}.start();
	}

	public CalendarEvent getEventById(long id) {
		for (CalendarEvent ce : events) {
			if (ce.getId().equals(String.valueOf(id)))
				return ce;
		}
		return null;
	}

	public void updateEvent(final CalendarEvent event, final RemoteCallBack rcb) {
		if (event == null)
			throw new IllegalArgumentException("Event cant be null here");

		new Thread() {
			@Override
			public void run() {
				super.run();

				ContentValues values = new ContentValues();
				values.put("title", event.getTitle());
				values.put("description", event.getDescription());
				values.put("eventLocation", event.getPlace());
				values.put("dtstart", event.getStartDate().getTime());
				values.put("dtend", event.getEndDate().getTime());

				context.getContentResolver().update(
						Uri.parse("content://com.android.calendar/events/"
								+ event.getId()), values, null, null);

				if (rcb != null)
					rcb.callBack();
			}
		}.start();
	}

	public static Calendar getMidnight(int daysFromToday) {
		Calendar date = new GregorianCalendar();
		// reset hour, minutes, seconds and millis
		date.set(Calendar.HOUR_OF_DAY, 0);
		date.set(Calendar.MINUTE, 0);
		date.set(Calendar.SECOND, 0);
		date.set(Calendar.MILLISECOND, 0);

		date.add(Calendar.DAY_OF_MONTH, daysFromToday);
		return date;
	}

	public Cursor getCursor() {
		return cursor;
	}

	public void setCursor(Cursor cursor) {
		this.cursor = cursor;
	}

	public interface RemoteCallBack {
		public void callBack();
	}

	public interface RemoteCursorCallBack {
		public void callBack(Cursor cursor);
	}

	public interface RemoteEventCallBack {
		public void callBack(CalendarEvent ce);
	}

}
